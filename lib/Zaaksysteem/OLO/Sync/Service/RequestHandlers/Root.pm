package Zaaksysteem::OLO::Sync::Service::RequestHandlers::Root;

use Moose;

with qw[
    Zaaksysteem::Service::HTTP::RequestHandler
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::OLO::Sync::Service::RequestHandlers::Root - Handle requests on
the root endpoint

=head1 DESCRIPTION

This requesthandler responds with a L<Syzygy::Object::Types::Service>
serialization for the OLO service.

=cut

use BTTW::Tools;
use Syzygy::Object::Model;
use URI;
use Zaaksysteem::OLO;

=head1 METHODS

=head2 name

Implements the name interface required by
L<Zaaksysteem::Service::HTTP::RequestHandler>.

Returns the empty string, this handler attaches to the root of the
application.

=cut

sub name { '' }

=head2 dispatch

Implements the dispatcher for the root handler. Returns a C<HashRef> of data
to be encoded in the response.

=cut

sub dispatch {
    my ($self, $request, $response) = @_;

    return Syzygy::Object::Model->new_object(service => {
        name => 'Zaaksysteem OLO synchronisation service',
        repository => URI->new('https://gitlab.com/zaaksysteem/zaaksysteem-olo-sync'),
        version => "$Zaaksysteem::OLO::VERSION"
    });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::OLO::Sync uses the EUPL license, for more information please have a look at the C<LICENSE> file.

